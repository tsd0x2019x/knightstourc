/*
 * Field.h
 *
 *  Created on: 07.04.2019
 *  Author: sefi16
 */

#ifndef FIELD_H
#define FIELD_H

typedef struct Field Field; // forward-declaration
struct Field {
	int row;
	int col;
};

#endif /* FIELD_H */
