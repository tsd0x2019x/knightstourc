/*
 * PossibleNextMoves.h
 *
 *  Created on: 07.04.2019
 *  Author: sefi16
 */

#ifndef POSSIBLENEXTMOVES_H
#define POSSIBLENEXTMOVES_H

#include <stdlib.h>
#include "Field.h"

/**
 * PossibleNextMovesList is a (global) list that contains many sub-lists of all next possible/valid/free fields
 * to move on from the current field.
 *
 * There is only one PossibleNextMovesList in the whole program. It contains zero, one or many sub-lists (named
 * PossibleNextMoves). Each of the <<PossibleNextMoves>> sub-lists contains one <<current>> field (i.e. the field
 * where the knight is currently on) and a sequence of the next possible/valid/free fields the knight can jump to
 * from the <<current>> field.
 *
 *                               PossibleNextMovesList (pnmList_t)
 *                                          |
 *                                          |
 *   _______________________________________V__________________________________________________
 *  |                                                                                          |
 *  |  +==============+        +==============+         +==============+                       |
 *  |  |   current    |        |   current    |         |   current    |                       |
 *  |  +==============+        +==============+         +==============+                       |
 *  |  |    [PNM 0]   |        |    [PNM 1]   |         |    [PNM 2]   |                       |
 *  |  |    field 0   |        |    field 0   |         |    field 0   |                       |
 *  |  |    field 1   |        |    field 1   |         |    field 1   |                       |
 *  |  |    field 2   |        |    field 2   |         |    field 2   |                       |
 *  |  |    ....      |======>>|    ....      |=======>>|    ....      |======>> ....          |
 *  |  +==============+  next  +==============+  next   +==============+  next                 |
 *  |__________________________________________________________________________________________|
 *
 * [ PNM ] : PossibleNextMoves (sub-list), pnm_t
 */


typedef struct PNode pnode_t; // forward-declaration
struct PNode {
	Field field;
	pnode_t * next;
};

// PossibleNextMoves (pnm_t) sub-list
typedef struct PNM pnm_t; // forward-declaration
struct PNM {
	pnode_t * begin;
	pnode_t * end;
};

// PossibleNextMovesListNode. Each node contains a pnm_t sub-list
typedef struct PNMLNode pnmListNode_t;
struct PNMLNode {
	Field current; // current field, where the knight is currently on
	pnm_t * pnmSubList; // pointer to pnm_t sub-list
	pnmListNode_t * next;
};

// PossibleNextMovesList
typedef struct PNMList pnmList_t;
struct PNMList {
	pnmListNode_t * begin;
	pnmListNode_t * end;
};

// Provides the size (length) of list
extern int PNM_Size(pnm_t * pnm);
extern int PNMList_Size(pnmList_t * pnmList);

// Insert a new element to position <index> in the list
extern int PNM_AddElement(pnm_t * pnm, pnode_t * newNode, int index);
extern int PNM_InsertAt(pnm_t * pnm, pnode_t * newNode, int index);
extern int PNMList_AddElement(pnmList_t * pnmList, pnmListNode_t * newNode, int index);
extern int PNMList_InsertAt(pnmList_t * pnm, pnmListNode_t * newNode, int index);

// Append a new element at list end
extern int PNM_Pushback(pnm_t * pnm, pnode_t * newNode);
extern int PNM_PushbackField(pnm_t * pnm, int iRow, int iCol);
extern int PNM_Append(pnm_t * pnm, pnode_t * newNode);
extern int PNM_AppendField(pnm_t * pnm, int iRow, int iCol);
extern int PNMList_Pushback(pnmList_t * pnm, pnmListNode_t * newNode);
extern int PNMList_Append(pnmList_t * pnm, pnmListNode_t * newNode);

// Provides the pointer (reference) to element at position <index>
extern pnode_t * PNM_GetElement(pnm_t * pnm, int index);
extern pnmListNode_t * PNMList_GetElement(pnmList_t * pnmList, int index);

extern int PNM_DeleteElement(pnm_t * pnm, int index);
extern int PNM_RemoveElement(pnm_t * pnm, int index);
extern pnode_t * PNM_DeleteElementAndGetReference(pnm_t * pnm, int index);
extern pnode_t * PNM_RemoveElementAndGetReference(pnm_t * pnm, int index);
extern int PNMList_DeleteElement(pnmList_t * pnmList, int index);
extern int PNMList_RemoveElement(pnmList_t * pnmList, int index);

extern int PNM_PopbackAndFree(pnm_t * pm);
extern pnode_t * PNM_Popback(pnm_t * pnm);
extern int PNMList_PopbackAndFree(pnmList_t * pmList);
extern pnmListNode_t * PNMList_Popback(pnmList_t * pnmList);

#endif /* POSSIBLENEXTMOVES_H */
