/*
 * PossibleNextMoves.c
 *
 *  Created on: 07.04.2019
 *  Author: sefi16
 */
#include "PossibleNextMovesList.h"


/*
 * Provides the size (length) of pnm_t sub-list
 */
int PNM_Size(pnm_t * pnm) {
	if (pnm == NULL) return -1; // Invalid list!
	if (pnm->begin == NULL) return 0; // Empty list
	pnode_t * next = pnm->begin;
	int n = 1;
	while (next != pnm->end) {
		next = next->next;
		++n;
	}
	return n;
}

/*
 * Provides the size (length) of pnmList_t list
 */
int PNMList_Size(pnmList_t * pnmList) {
	if (pnmList == NULL) return -1; // Invalid list!
	if (pnmList->begin == NULL) return 0; // Empty list
	pnmListNode_t * next = pnmList->begin;
	int n = 1;
	while (next != pnmList->end) {
		next = next->next;
		++n;
	}
	return n;
}

/*
 * Insert a new element at position <index> in the pnm_t sub-list
 */
int PNM_AddElement(pnm_t * pnm, pnode_t * newNode, int index) {
	int iSize = PNM_Size(pnm);
	if (iSize < 0 || index < 0) return -1; // Invalid list => Nothing to be inserted
	else { // iSize >= 0 && index >= 0
		if (iSize == 0) {
			if (index != 0) return -1; // OutOfSize => Nothing inserted
			else { // index == 0
				pnm->begin = newNode;
				pnm->end = newNode;
				newNode->next = NULL;
			}
		}
		else { // iSize > 0
			if (index >= iSize) return -1; // OutOfSize => Nothing inserted
			else { // index < iSize
				if (index == 0) {
					newNode->next = pnm->begin;
					pnm->begin = newNode;
				}
				else { // index > 0
					pnode_t * pNext = pnm->begin; // index 0
					int i = 0;
					while (i < (index-1)) {
						pNext = pNext->next;
						++i;
					}
					newNode->next = pNext->next;
					pNext->next = newNode;
				}
			}
		}
	}
	return 0;
}

/*
 *
 */
int PNM_InsertAt(pnm_t * pnm, pnode_t * newNode, int index) {
	return PNM_AddElement(pnm, newNode, index);
}

/*
 * Insert a new element at position <index> in the primary list
 */
int PNMList_AddElement(pnmList_t * pnmList, pnmListNode_t * newNode, int index) {
	int iSize = PNMList_Size(pnmList);
		if (iSize < 0 || index < 0) return -1; // Invalid list => Nothing to be inserted
		else { // iSize >= 0 && index >= 0
			if (iSize == 0) {
				if (index != 0) return -1; // OutOfSize => Nothing inserted
				else { // index == 0
					pnmList->begin = newNode;
					pnmList->end = newNode;
					newNode->next = NULL;
				}
			}
			else { // iSize > 0
				if (index >= iSize) return -1; // OutOfSize => Nothing inserted
				else { // index < iSize
					if (index == 0) {
						newNode->next = pnmList->begin;
						pnmList->begin = newNode;
					}
					else { // index > 0
						pnmListNode_t * pNext = pnmList->begin; // index 0
						int i = 0;
						while (i < (index-1)) {
							pNext = pNext->next;
							++i;
						}
						newNode->next = pNext->next;
						pNext->next = newNode;
					}
				}
			}
		}
		return 0;
}

/*
 *
 */
int PNMList_InsertAt(pnmList_t * pnmList, pnmListNode_t * newNode, int index) {
	return PNMList_AddElement(pnmList, newNode, index);
}

/*
 *
 */
int PNM_Pushback(pnm_t * pnm, pnode_t * newNode) {
	int iSize = PNM_Size(pnm);
	if (iSize < 0 || newNode == NULL) return -1; // Invalid => Nothing inserted
	if (iSize == 0) {
		pnm->begin = newNode;
		pnm->end = newNode;
		newNode->next = NULL;
	}
	else { // iSize > 0
		pnm->end->next = newNode;
		pnm->end = newNode;
		newNode->next = NULL;
	}
	return 0;
}

/*
 *
 */
int PNM_PushbackField(pnm_t * pnm, int iRow, int iCol) {
	int iSize = PNM_Size(pnm);
	if (iSize < 0) return -1; // Invalid => Nothing inserted
	if (iSize == 0) {
		pnm->begin = (pnode_t *) malloc(sizeof(pnode_t));
		pnm->begin->field.col = iCol;
		pnm->begin->field.row = iRow;
		pnm->begin->next = NULL;
		pnm->end = pnm->begin;
	}
	else { // iSize > 0
		pnm->end->next = (pnode_t *) malloc(sizeof(pnode_t));
		pnm->end->next->field.col = iCol;
		pnm->end->next->field.row = iRow;
		pnm->end->next->next = NULL;
		pnm->end = pnm->end->next;
	}
	return 1;
}

/*
 *
 */
int PNM_Append(pnm_t * pnm, pnode_t * newNode) {
	return PNM_Pushback(pnm,newNode);
}

/*
 *
 */
int PNM_AppendField(pnm_t * pnm, int iRow, int iCol) {
	return PNM_PushbackField(pnm, iRow, iCol);
}

/*
 *
 */
int PNMList_Pushback(pnmList_t * pnmList, pnmListNode_t * newNode) {
	int iSize = PNMList_Size(pnmList);
	if (iSize < 0 || newNode == NULL) return -1; // Invalid => Nothing inserted
	if (iSize == 0) {
		pnmList->begin = newNode;
		pnmList->end = newNode;
		newNode->next = NULL;
	}
	else { // iSize > 0
		pnmList->end->next = newNode;
		pnmList->end = newNode;
		newNode->next = NULL;
	}
	return 0;
}

/*
 *
 */
int PNMList_Append(pnmList_t * pnm, pnmListNode_t * newNode) {
	return PNMList_Pushback(pnm,newNode);
}

/*
 * Provides the pointer (reference) to element at position <index>
 */
pnode_t * PNM_GetElement(pnm_t * pnm, int index) {
	int iSize = PNM_Size(pnm);
	if (iSize <= 0 || index < 0 || index >= iSize) return NULL;
	if (index == (iSize-1)) return pnm->end;
	pnode_t * pElement = pnm->begin;
	for(int i = 0; i < index; ++i) pElement = pElement->next;
	return pElement;
}

/*
 *
 */
pnmListNode_t * PNMList_GetElement(pnmList_t * pnmList, int index) {
	int iSize = PNMList_Size(pnmList);
		if (iSize <= 0 || index < 0 || index >= iSize) return NULL;
		if (index == (iSize-1)) return pnmList->end;
		pnmListNode_t * pElement = pnmList->begin;
		for(int i = 0; i < index; ++i) pElement = pElement->next;
		return pElement;
}

/*
 * Removes an element from sub-list and returns the pointer (reference) to it.
 * Furthermore, the allocated memory for this (removed) element will be freed.
 */
int PNM_DeleteElement(pnm_t * pnm, int index) {
	int iSize = PNM_Size(pnm);
	if (iSize <= 0 || index < 0 || index >= iSize) return -1; // Invalid => Nothing is deleted
	if (iSize == 1) { // && (index == 0)
		free(pnm->begin);
		pnm->begin = NULL;
		pnm->end = NULL;
	}
	else { // iSize > 1
		pnode_t * pElement = pnm->begin;
		for(int i = 0; i < (index-1); ++i) pElement = pElement->next;
		pnode_t * pTemp = pElement->next;
		pElement->next = pElement->next->next;
		free(pTemp);
		pTemp = NULL;
	}
	return 1;
}

/*
 * Removes an element from sub-list and returns the pointer (reference) to it.
 * This method does not free the allocated memory of this element's pointer.
 */
int PNM_RemoveElement(pnm_t * pnm, int index) {
	return PNM_DeleteElement(pnm, index);
}

/*
 * Removes an element from sub-list and returns the pointer (reference) to it.
 * Warning: this method does not free the allocated memory for that (removed) element.
 */
pnode_t * PNM_DeleteElementAndGetReference(pnm_t * pnm, int index) {
	int iSize = PNM_Size(pnm);
	if (iSize <= 0 || index < 0 || index >= iSize) return NULL; // Invalid => Nothing is deleted
	pnode_t * pNode = NULL; // the element at [index] to be deleted
	if (iSize == 1) { // && (index == 0)
		pNode = pnm->begin;
		pnm->begin = NULL;
		pnm->end = NULL;
	}
	else { // iSize > 1
		pnode_t * pElemPrior = pnm->begin; // the element (at [index-1]) immediately prior to the element to be deleted.
		for(int i = 1; i < index; ++i) { // tries to get the element at [index-1]
			pElemPrior = pElemPrior->next;
		} // the element at [index-1] is pElemPrior
		pNode = pElemPrior->next; // get the element at [index]
		if (pNode != pnm->end) {
			pElemPrior->next = pNode->next;
		}
		else { // pNode == pnm->end
			pNode = pnm->end;
			pnm->end = pElemPrior;
			pnm->end->next = NULL;
			pElemPrior = NULL;
		}
		pNode->next = NULL;
	}
	return pNode;
}

/*
 * Removes an element from sub-list and returns the pointer (reference) to it.
 * Warning: this method does not free the allocated memory for that (removed) element.
 */
pnode_t * PNM_RemoveElementAndGetReference(pnm_t * pnm, int index) {
	return PNM_DeleteElementAndGetReference(pnm, index);
}

/*
 *
 */
int PNMList_DeleteElement(pnmList_t * pnmList, int index) {
	int iSize = PNMList_Size(pnmList);
	if (iSize <= 0 || index < 0 || index >= iSize) return -1; // Invalid => Nothing is deleted
	if (iSize == 1) { // && (index == 0)
		free(pnmList->begin);
		pnmList->begin = NULL;
		pnmList->end = NULL;
	}
	else { // iSize > 1
		pnmListNode_t * pElement = pnmList->begin;
		for(int i = 0; i < (index-1); ++i) pElement = pElement->next;
		pnmListNode_t * pTemp = pElement->next;
		pElement->next = pElement->next->next;
		free(pTemp);
		pTemp = NULL;
	}
	return 1;
}

/*
 *
 */
int PNMList_RemoveElement(pnmList_t * pnmList, int index) {
	return PNMList_DeleteElement(pnmList, index);
}

/*
 * Pop/remove the last element from (sub-)list. The allocated memory for that (removed) element will be freed.
 */
int PNM_PopbackAndFree(pnm_t * pnm) {
	int iSize = PNM_Size(pnm);
	if (iSize <= 0) return -1; // Invalid => Nothing is deleted
	if (iSize == 1) {
		pnm->end = NULL;
		free(pnm->begin);
		pnm->begin = NULL;
	}
	else { // iSize > 1
		pnode_t * pElement = pnm->begin;
		for(int i = 0; i < (iSize-2); ++i) pElement = pElement->next; // Iterate to the next-to-last element
		pElement->next = NULL;
		free(pnm->end);
		pnm->end = pElement;
	}
	return 1;
}

/*
 * Pop/delete the last element from pnm_t (sub-)list and return the pointer/reference to it
 */
pnode_t * PNM_Popback(pnm_t * pnm) {
	int iSize = PNM_Size(pnm);
	if (iSize <= 0) return NULL; // Invalid => Nothing is deleted
	pnode_t * pTemp = pnm->end;
	if (iSize == 1) {
		pnm->end = NULL;
		pnm->begin = NULL;
	}
	else { // iSize > 1
		pnode_t * pElement = pnm->begin;
		int i = 0;
//		for(i = 0; i < (iSize-1); ++i) pElement = pElement->next; // Iterate to the next-to-last element
//		pElement->next = NULL;
//		pnm->end = pElement;
		while (i < (iSize-2)) {
			pElement = pElement->next;
			++i;
		}
		pnm->end = pElement;
		pElement->next = NULL;
		pElement = NULL;
	}
	return pTemp; // The allocated memory has to be freed (free(pTemp)) in the following instruction.
}

/*
 * Pop/remove the last element from pnmList_t list. The allocated memory for that (removed) element will be freed.
 */
int PNMList_PopbackAndFree(pnmList_t * pnmList) {
	int iSize = PNMList_Size(pnmList);
	if (iSize <= 0) return -1; // Invalid => Nothing is deleted
	if (iSize == 1) {
		pnmList->end = NULL;
		free(pnmList->begin);
		pnmList->begin = NULL;
	}
	else { // iSize > 1
		pnmListNode_t * pElement = pnmList->begin;
		for(int i = 0; i != (iSize-2); ++i) pElement = pElement->next; // Iterate to the next-to-last element
		pElement->next = NULL;
		free(pnmList->end);
		pnmList->end = pElement;
	}
	return 1;
}

/*
 * Pop/delete the last element from pnmList_t list and return the pointer/reference to it
 */
pnmListNode_t * PNMList_Popback(pnmList_t * pnmList) {
	int iSize = PNMList_Size(pnmList);
	if (iSize <= 0) return NULL; // Invalid => Nothing is deleted
	pnmListNode_t * pTemp = pnmList->end;
	if (iSize == 1) {
		pnmList->end = NULL;
		pnmList->begin = NULL;
	}
	else { // iSize > 1
		pnmListNode_t * pElement = pnmList->begin;
		for(int i = 0; i < (iSize-2); ++i) pElement = pElement->next; // Iterate to the next-to-last element
		pElement->next = NULL;
		pnmList->end = pElement;
	}
	return pTemp; // The allocated memory has to be freed (free(pTemp)) in the following instruction.
}
