/*
 * Main.c
 *
 *  Created on: 07.04.2019
 *  Author: sefi16
 */
#include <stdio.h>
#include <stdlib.h> // For malloc and free
#include <stdarg.h> // For using va_list (variable argument number): https://www.tutorialspoint.com/cprogramming/c_variable_arguments.htm
#include "utils/Field.h"
#include "utils/PossibleNextMovesList.h"

/*
 * Für die Berechnung der Array-Länge wird Präprozessor (Makro) verwendet.
 * Weil mit einer Funktion, z.B. "int Array_Length(int A[])" oder "int Array_Length(int * A)"
 * nur die Größe des Pointers auf Array zurückgegeben wird, und nicht die gewünschte Arraygröße.
 * In beiden Fällen (int A[]) und (int * A) wird A ausschließlich als Pointer auf das erste
 * Arrayelement übergeben. Und sizeof(A) würde nicht die gesamte Byteanzahl aller Array-
 * elemente liefern, sondern lediglich die Pointergröße (z.B. 8).
 * Aber dieses Verfahren funktioniert nur mit Array der Dimension n=1. Für n>=2 scheitert es.
 */
#define Array_Length(A) sizeof(A)/sizeof(A[0])
#define FALSE 0
#define TRUE  1
#define _FREE_ 0
#define _NOFREE_ 1
#define DEBUG(x) ( ((x) == TRUE) ? (PrintBoard(board)) : (funcPseudo()) )
// Remember to always put your define between parentheses (x), to avoid syntax error when replacing.

/* Global variables */
int DEBG_OPT = FALSE; // Debug option. If TRUE, the current board state will be printed. Otherwise, nothing's shown.
int FIRST_CALL_FINDPATH = TRUE;
int BOARD_HEIGHT = 0;
int BOARD_WIDTH = 0;
int ** m_board = NULL; // Pointer to an array of pointers (as 2-dimensional array)
pnmList_t * m_pnmList = NULL; // Global list of next possible/valid moves (fields)

static unsigned long m_Count = 0;

/* Function declaration */
void UnsetField(int iRow, int iCol, int* board[]);
void SetField(int iRow, int iCol, int* board[]);
int IsValidField(int iRow, int iCol, int* board[]);
unsigned int GetNumberOfPossibleNextMovesFromField(int iRow, int iCol, int ** board);
void SearchPossibleNextMovesFromField(int iRow, int iCol, int ** board, pnm_t * pnm);
void PrintBoard(int ** board);
int Backtracking_Warndorf(int ** board);
int Findpath_Warndorf(int ** board, int iRow, int iCol);
void FreeMemory();
void funcSetBoard(int ** board, int num, ...);


void funcPseudo() {}


/* Function definition */

void UnsetField(int iRow, int iCol, int* board[]) {
	board[iRow][iCol] = _FREE_;
}

void SetField(int iRow, int iCol, int* board[]) {
	board[iRow][iCol] = _NOFREE_;
}

int IsValidField(int iRow, int iCol, int* board[]) {
	if (iRow < 0 || iRow >= BOARD_HEIGHT || iCol < 0 || iCol >= BOARD_WIDTH) return FALSE; // ArrayOutOfRange exception
	if (board[iRow][iCol] != _FREE_) return FALSE; // Field's not free
	return TRUE;
}

unsigned int GetNumberOfPossibleNextMovesFromField(int iRow, int iCol, int ** board) {
	unsigned int number = 0;

	// 1st possible next move
	if (IsValidField(iRow-2, iCol-1, board)) ++number;
	// 2nd possible next move
	if (IsValidField(iRow-1, iCol-2, board)) ++number;
	// 3rd possible next move
	if (IsValidField(iRow+1, iCol-2, board)) ++number;
	// 4th possible next move
	if (IsValidField(iRow+2, iCol-1, board)) ++number;
	// 5th possible next move
	if (IsValidField(iRow+2, iCol+1, board)) ++number;
	// 6th possible next move
	if (IsValidField(iRow+1, iCol+2, board)) ++number;
	// 7th possible next move
	if (IsValidField(iRow-1, iCol+2, board)) ++number;
	// 8th possible next move
	if (IsValidField(iRow-2, iCol+1, board)) ++number;

	return number;
}

void SearchPossibleNextMovesFromField(int iRow, int iCol, int ** board, pnm_t * pnm) {
	// 1st possible next move
	if (IsValidField(iRow-2, iCol-1, board)) {
		PNM_PushbackField(pnm, iRow-2, iCol-1);
	}
	// 2nd possible next move
	if (IsValidField(iRow-1, iCol-2, board)) {
		PNM_PushbackField(pnm, iRow-1, iCol-2);
	}
	// 3rd possible next move
	if (IsValidField(iRow+1, iCol-2, board)) {
		PNM_PushbackField(pnm, iRow+1, iCol-2);
	}
	// 4th possible next move
	if (IsValidField(iRow+2, iCol-1, board)) {
		PNM_PushbackField(pnm, iRow+2, iCol-1);
	}
	// 5th possible next move
	if (IsValidField(iRow+2, iCol+1, board)) {
		PNM_PushbackField(pnm, iRow+2, iCol+1);
	}
	// 6th possible next move
	if (IsValidField(iRow+1, iCol+2, board)) {
		PNM_PushbackField(pnm, iRow+1, iCol+2);
	}
	// 7th possible next move
	if (IsValidField(iRow-1, iCol+2, board)) {
		PNM_PushbackField(pnm, iRow-1, iCol+2);
	}
	// 8th possible next move
	if (IsValidField(iRow-2, iCol+1, board)) {
		PNM_PushbackField(pnm, iRow-2, iCol+1);
	}
}

/*
 * Display the current state of the (chess) board
 */
void PrintBoard(int ** board) {
	for(int i = 0; i < BOARD_HEIGHT; ++i) {
		for(int j = 0; j < BOARD_WIDTH; ++j) {
			printf("%d ", board[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

/*
 * Display the found path for knight's tour
 */
void PrintPath() {
	if (m_pnmList == NULL) return;
	pnmListNode_t * pnmListNode = m_pnmList->begin;
	printf("==> Path: ");
	while (pnmListNode != NULL) {
		printf("(%d,%d)->", pnmListNode->current.row, pnmListNode->current.col);
		pnmListNode = pnmListNode->next;
	}
}

/*
 * Bachtracking using Warndorf's rule
 */
int Backtracking_Warndorf(int ** board) {

	printf("\nBacktracking_Warndorf:: count = %lu\n", m_Count);
	m_Count += 1;

	int iListSize = PNMList_Size(m_pnmList);

	if (iListSize == 0) { // There is no path !!
		printf("\nNo Path...\n");
		DEBUG(DEBG_OPT);
		return FALSE;
	}
	else { // iListSize > 0
		// Pick a node from the pnmList_t list => Jump back to a previous field.
		pnmListNode_t * pnmListNode = PNMList_Popback(m_pnmList);

		int iSubListSize = PNM_Size(pnmListNode->pnmSubList);

		if (iSubListSize > 0) {
			// Search for the field (node) from which there are least number of possible/valid next moves
			int i = 0;
			int MIN = 9;
			int minIndex = 0;
			pnode_t * pNode = pnmListNode->pnmSubList->begin;
			while (pNode != NULL) {
				int num = GetNumberOfPossibleNextMovesFromField(pNode->field.row, pNode->field.col, board);
				if(num != 0 && num < MIN) {
					MIN = num;
					minIndex = i;
				}
				++i;
				pNode = pNode->next;
			}
			pNode = PNM_RemoveElementAndGetReference(pnmListNode->pnmSubList, minIndex);
			PNMList_Pushback(m_pnmList, pnmListNode);
			int iNextRow = pNode->field.row;
			int iNextCol = pNode->field.col;
			free(pNode);
			pNode = NULL;
			return Findpath_Warndorf(board, iNextRow, iNextCol);
		}
		else if (iSubListSize == -1 ) {
			printf("\nBacktracking_Warndorf.iSubListSize = -1\n");
			return FALSE;
		}
		else { // PNM_Size(pnmListNode->pnmSubList) == 0
			UnsetField(pnmListNode->current.row, pnmListNode->current.col, board);
			DEBUG(DEBG_OPT);
			free(pnmListNode->pnmSubList);
			free(pnmListNode);
			pnmListNode->pnmSubList = NULL;
			pnmListNode->next = NULL;
			pnmListNode = NULL;
			return Backtracking_Warndorf(board);
		}
	}
}

/*
 * Finds the path for the knight (Springer) using Warndorf's rule
 */
int Findpath_Warndorf(int ** board, int iRow, int iCol) {

	printf("\nFindpath_Warndorf:: count = %lu\n", m_Count);
	m_Count += 1;

	if (!IsValidField(iRow, iCol, board)) {
		return FALSE;
	}

	// Create the global list of all next possible/valid fields to move on
	if (FIRST_CALL_FINDPATH) {
		m_pnmList = (pnmList_t *) malloc(sizeof(pnmList_t)); // Create global list
		FIRST_CALL_FINDPATH = FALSE;
	}

	SetField(iRow, iCol, board); // Mark the field
	DEBUG(DEBG_OPT);

	// Create the sub-list of next possible fields to move on from current field (iRow;iCol)
	pnmListNode_t * pnmListNode = (pnmListNode_t *) malloc(sizeof(pnmListNode_t));
	// Remember current field
	pnmListNode->current.row = iRow;
	pnmListNode->current.col = iCol;
	// Initialize pnm_t sub-list
	pnmListNode->pnmSubList = (pnm_t *) malloc(sizeof(pnm_t));
	pnmListNode->pnmSubList->begin = NULL;
	pnmListNode->pnmSubList->end = NULL;
	pnmListNode->next = NULL;

	// Search for the next possible/valid fields to move on
	SearchPossibleNextMovesFromField(iRow, iCol, board, pnmListNode->pnmSubList);

	int iSubListSize = PNM_Size(pnmListNode->pnmSubList);

	if (iSubListSize > 0) { // There are valid fields to move
		// Search for and pick the node from which there are least number of possible/valid next moves
		int i = 0;
		int MIN = 9;
		int minIndex = 0;
		pnode_t * pNode = pnmListNode->pnmSubList->begin;
		while (pNode != NULL) {
			int num = GetNumberOfPossibleNextMovesFromField(pNode->field.row, pNode->field.col, board);
			if(num != 0 && num < MIN) {
				MIN = num;
				minIndex = i;
			}
			++i;
			pNode = pNode->next;
		}
		pNode = PNM_RemoveElementAndGetReference(pnmListNode->pnmSubList, minIndex);
		PNMList_Pushback(m_pnmList, pnmListNode);
		int iNextRow = pNode->field.row;
		int iNextCol = pNode->field.col;
		free(pNode);
		pNode = NULL;
		return Findpath_Warndorf(board, iNextRow, iNextCol);
	}
	else { // PNM_Size(pnmListNode->pnmSubList) == 0, there are no further fields => either path is found or dead-end (no path)
		if (PNMList_Size(m_pnmList) == (BOARD_HEIGHT * BOARD_WIDTH - 1)) { // Path is found
			PNMList_Pushback(m_pnmList, pnmListNode);
			return TRUE;
		}
		else { // Still no path yet => Backtracking
			UnsetField(iRow, iCol, board); // Unmark the field
			DEBUG(DEBG_OPT);
			free(pnmListNode->pnmSubList);
			free(pnmListNode);
			pnmListNode->pnmSubList = NULL;
			pnmListNode = NULL;
			return Backtracking_Warndorf(board);
		}
	}
}

/*
 * Free all allocated memory
 */
void FreeMemory () {

	if (m_pnmList == NULL) return;

	// Free all allocated memory spaces for pnmList
	pnmListNode_t * pnmListNode = m_pnmList->begin;
	pnmListNode_t * pnmListNodeTemp = NULL;
	while (pnmListNode != NULL) {
		pnm_t * pnmSubList = pnmListNode->pnmSubList;
		pnode_t * pNode = pnmSubList->begin;
		pnode_t * pNodeTemp = NULL;
		while (pNode != NULL) {
			pNodeTemp = pNode;
			pNode = pNode->next;
			free(pNodeTemp);
			pNodeTemp = NULL;
		}
		pnmSubList->begin = NULL;
		pnmSubList->end = NULL;
		free(pnmSubList);
		pnmListNodeTemp = pnmListNode;
		pnmListNode = pnmListNode->next;
		free(pnmListNodeTemp);
		pnmListNodeTemp = NULL;
	}
	m_pnmList->begin = NULL;
	m_pnmList->end = NULL;
	free(m_pnmList);
	m_pnmList = NULL;

	// Free allocated memory of chess board
	if (m_board == NULL) return;
	for(int i = 0; i < BOARD_HEIGHT; ++i) {
		free(m_board[i]);
	}
	m_board = NULL;
}

/*
 * Using va_list (variable argument number) with ellipses (three dots ...)
 * 		https://www.tutorialspoint.com/cprogramming/c_variable_arguments.htm
 */
void funcSetBoard(int ** board, int num, ...) {

	va_list valist;

	// Initialize va_list for variable number of arguments
	va_start(valist, num);

	// Access all the arguments assigned to the va_list
	int i = 0;
	while (i != num) {
		int iRow = va_arg(valist, int);
		++i;
		int iCol = va_arg(valist, int);
		++i;
		SetField(iRow, iCol, board);
	}

	// Clean memory reserved for va_list
	va_end(valist);
}


/*
 * Main Program
 */
int main_() {

	// Dimension of (chess) board
	BOARD_HEIGHT = 5;
	BOARD_WIDTH = 5;

	int* chessboard[BOARD_HEIGHT]; // Declare an two-dimensional array (as array of pointers)
	m_board = chessboard;

	// Allocate memory space for each pointer (element) of array
	for(int i = 0; i < BOARD_HEIGHT; ++i) {
		m_board[i] = (int*) malloc(BOARD_WIDTH * sizeof(int));
	}
	// Initialize elements
	for(int i = 0; i < BOARD_HEIGHT; ++i) {
		for(int j = 0; j < BOARD_WIDTH; ++j) {
			m_board[i][j] = _FREE_; // free
		}
	}

	printf("\nBefore knight's tour:\n");
	PrintBoard(m_board); // Before knight's tour

//	funcSetBoard(m_board, 12,	0,0, 0,1, 1,0, 1,2, 2,0, 2,2);
//	funcSetBoard(m_board, 2,	1,2);
//	printBoard(m_board);

	// Start pathfinder
	printf("Running backtracking algorithm:\n");
	int IsPathAvaiable = Findpath_Warndorf(m_board, 0, 1);

	printf("\nAfter knight's tour:\n");
	PrintBoard(m_board); // Before knight's tour

	if (IsPathAvaiable == TRUE) {
		printf("\n==> YES. There's a path for knight's tour.\n");
		PrintPath();
	}
	else {
		printf("\n==> NO. There's no path.\n");
	}

	// Free all allocated memory
	FreeMemory();

	return 0;
}


