/*
 * Test.c
 *
 *  Created on: 28.04.2019
 *  Author: sefi16
 */
#include <stdio.h>

static int m_Count = 0;

void printCounter() {
	printf("Counter i : %d\n", m_Count);
	if (m_Count < 124780) {
		m_Count += 1;
		printCounter();
	}
}

int main() {

	printCounter();
	printf("\nFinish!!!\n");

	return 0;
}


