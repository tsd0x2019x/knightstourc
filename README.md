# KnightsTourC
Implementation of the Knight's tour problem (Springerproblem) in C

<b>Description</b>: Knight's tour problem<br />
https://en.wikipedia.org/wiki/Knight%27s_tour
<p></p>

<b>Beschreibung</b>: Springerproblem<br />
https://de.wikipedia.org/wiki/Springerproblem

<b>Remove old binary</b>:

    $ make clean

<b>Compile</b>:

    $ make

<b>Execute</b>:

1. Windows:
	1.1 Run "StartKT.exe" followed by 3 parameters for [dimension = 3], [row = 0], [column = 0] and/without the option [--debug | -d]:
    
            $ StartKT.exe 3 0 0 --debug
            $ StartKT.exe 3 0 0 --d
            $ StartKT.exe 3 0 0
			
2. Linux (Ubuntu):
	2.1 Run the binary "./StartKT" followed by 3 parameters for [dimension = 3], [row = 0], [column = 0] and/without the option [--debug | -d]:
    
            $ ./StartKT 3 0 0 --debug
            $ ./StartKT 3 0 0 --d
            $ ./StartKT 3 0 0