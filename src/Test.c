/**
 * Test.c
 */
#include <stdio.h>
#include <stdlib.h>


int main () {

    int * p = NULL;
    int * q = NULL;

    p = (int *) malloc(sizeof(int));
    *p = 2;

    q = (int *) malloc(sizeof(int));
    *q = 3;

    printf("p: %d\n", *p);
    printf("q: %d\n", *q);

    free(p);

    p = q;

    printf("p: %d\n", *p);
    printf("q: %d\n", *q);

    free(p);
    // free(q); // Segmentation fault!
    p = q = NULL;

    return 0;
}