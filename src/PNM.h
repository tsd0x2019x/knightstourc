/**
 * PNM.h (Possible Next Moves)
 */
#ifndef PNM_H
#define PNM_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PNM_API extern

//================================================================================================================================
/**
 * PossibleNextMovesList (PNM_List) is a (global) list that contains many sub-lists (PNM_SubList) of all next possible/valid/free 
 * fields to move on from the current field.
 *
 * There is only one PNM_List in the whole program. It contains zero, one or many sub-lists (named PNM_SubList). Each of the 
 * <<PNM_SubList>> sub-lists is a structure that contains one <<current>> field (i.e. the field where the knight is currently on) 
 * and a sequence (list) of the next possible/valid/free fields which the knight can jump to from the <<current>> field.
 *
 *                                       PNM_List
 *                                          |
 *                                          |
 *   _______________________________________V__________________________________________________
 *  |                                                                                          |
 *  |  <<PNM_SubList 0>>       <<PNM_SubList 1>>        <<PNM_SubList 2>>     ....             |
 *  |  +==============+        +==============+         +==============+                       |
 *  |  |   current    |        |   current    |         |   current    |                       |
 *  |  +==============+        +==============+         +==============+                       |
 *  |  |              |        |              |         |              |                       |
 *  |  |    field 0   |        |    field 0   |         |    field 0   |                       |
 *  |  |    field 1   |        |    field 1   |         |    field 1   |                       |
 *  |  |    field 2   |        |    field 2   |         |    field 2   |                       |
 *  |  |    ....      |======>>|    ....      |=======>>|    ....      |======>> ....          |
 *  |  +==============+  next  +==============+  next   +==============+  next                 |
 *  |__________________________________________________________________________________________|
 * 
 *  
 *  Each <<PNM_SubList>> is a structure that contains one <<current>> field and an (internal) list of the so called
 *  possible/valid/free fields which the knight figure being on the <<current>> field can move to.
 * 
 * 
 *     PNN_SubList                   (Internal) List of           PNM_FNode               PNM_Field
 *         |                         possible next moves              |                      |
 *         |                                 |                        |                      |
 *         |                                 |                        |                      |
 *         |                           PNM_FNode_List                 |                      |
 *         V                                 |                        |                      |
 *  +==============+                         |                        |                      |
 *  |   current    |                         V                        |                      |
 *  +==============+                  +==============+                |                      |
 *  |              |                  |              |          ______V_______               V
 *  |    field 0   |                  |    field 0   |         |    field 0   |            field 0
 *  |    field 1   |                  |    field 1   |         +==============+
 *  |    field 2   |                  |    field 2   |                |
 *  |              |                  |              |                | next
 *  |    ....      |======>>          |    ....      |                V
 *  +==============+  next            +==============+
 *
 */
typedef struct { // represents a Field with its coordinates (x,y) = (row,column)
    int row; 
    int col; 
} PNM_Field;

typedef struct pnm_fnode_t PNM_FNode; // forward declaration
struct pnm_fnode_t { // represents a Node (struct) that contains a Field and a pointer to the next Node
    PNM_Field field;
    PNM_FNode * next;
};

typedef struct { // represents an internal list of possible/next/free fields for knight to move to. Is a sub-list of <<PNM_SubList>> and a sub-sub-list of <<PNM_List>>.
    unsigned int size;
    PNM_FNode * begin;
    PNM_FNode * end;
} PNM_FNode_List;

typedef struct pnm_sublist_t PNM_SubList; // forward declaration
struct pnm_sublist_t { // represents every sub-list of the unique (global) list <<PNM_List>>
    PNM_Field current;
    PNM_FNode_List * nodeList;
    PNM_SubList * next;
};

typedef struct { // represents the unique (global) list for all possible next moves
    unsigned int size;
    PNM_SubList * begin;
    PNM_SubList * end;
} PNM_List;

//==============================================================================================================================

PNM_API int              PNM_FNode_List_AddField (PNM_FNode_List * list, int row, int col);
PNM_API PNM_FNode *      PNM_FNode_List_RemoveNodeAtIndex (PNM_FNode_List * list, int index);
PNM_API PNM_SubList *    PNM_SubList_CreateNewInstance (int row, int col, unsigned short flag_malloc_nodeList);
PNM_API PNM_FNode_List * PNM_SubList_CreateAndInitalizeNewNodeList (PNM_SubList * list);
PNM_API int              PNM_List_AddSubList (PNM_List * list, PNM_SubList * new_sublist);
PNM_API PNM_SubList *    PNM_List_Popback (PNM_List * list);
PNM_API int              PNM_List_Pushback (PNM_List * list, PNM_SubList * new_sublist);

//==============================================================================================================================
/*
 * Inserts/Appends a new element/field (PNM_FNode) at list end. Returns the current number of elements in the list.
 * @params:
 *      [list] : Sub-sub-list containing the current possible next moves from a certain field given by (row,col)
 *      [row] : Row index of the field to be inserted in the list
 *      [col] : Column index of the field to be inserted in the list
 */
PNM_API int PNM_FNode_List_AddField (PNM_FNode_List * list, int row, int col) {
    if (list == NULL) {
        return 0;
    }
    PNM_FNode * newField = (PNM_FNode *) malloc(sizeof(PNM_FNode));
    newField->field.row = row;
    newField->field.col = col;
    if (list->size == 0) {
        list->begin = newField;
        list->end = newField;
    }
    else {
        list->end->next = newField;
        list->end = newField;
    }
    list->end->next = NULL;
    ++(list->size);
    return list->size;
}

/*
 * Remove an element (PNM_FNode) at index [index] from the lsit (PNM_FNode_List).
 * Return a reference/pointer to that recently removed element. Note that the allocated
 * memory for this element still remains and is not freed, until the user does release it.
 * @params:
 *      [list] : A reference/pointer to the PNM_FNode_List to be looked up
 *      [index] : Index of the element (PNM_FNode) to be removed
 * @returns:
 *      Pointer to the recently removed element. Note that the allocated memory for this 
 *      removed element still remains and is not freed, until the user does release it.
 */
PNM_API PNM_FNode * PNM_FNode_List_RemoveNodeAtIndex (PNM_FNode_List * list, int index) {
    if (list == NULL || list->size == 0 || index < 0 || index >= list->size) {
        return NULL;
    }
    PNM_FNode * fnode = list->begin;
    if (list->size == 1) {
        list->begin = NULL;
        list->end = list->begin;
    }
    else { // list->size > 1
        if (index == 0) {
            list->begin = list->begin->next;            
        }
        else { // 0 < index < (list->size)
            int k = 0;
            while (k != index-1) { // Attempt to get the element [index-1], previous to the element [index]
                fnode = fnode->next;
                ++k;
            }
            // Reach the previous element [index-1]
            PNM_FNode * fnodeTemp = fnode;
            fnode = fnode->next;
            fnodeTemp->next = fnodeTemp->next->next;
        }
    }
    fnode->next = NULL;
    --(list->size);
    return fnode;
}

/*
 * Create a new sub-list (PNM_SubList) object with current row and column. If the flag [flag_malloc_nodeList] is set (1), memory
 * will be allocated for a new nodeList (PNM_FNode_List) object in this sub-list. If this flag is not set (0), there is no
 * memory allocation and the pointer of nodeList refers to NULL.
 */
PNM_API PNM_SubList * PNM_SubList_CreateNewInstance (int row, int col, unsigned short flag_malloc_nodeList) {
    PNM_SubList * subList = (PNM_SubList *) malloc(sizeof(PNM_SubList));
    subList->current.row = row;
    subList->current.col = col;
    if (flag_malloc_nodeList == 0) { 
        subList->nodeList = NULL;
    }
    else {
        subList->nodeList = (PNM_FNode_List *) malloc(sizeof(PNM_FNode_List));
        subList->nodeList->size = 0;
        subList->nodeList->begin = NULL;
        subList->nodeList->end = subList->nodeList->begin;
    }
    subList->next = NULL;
    return subList;
}

/*
 * Create and initialize a new nodeList for the given sub-list (PNM_SubList) object.
 * Return a reference/pointer to that just created nodeList. 
 */
PNM_API PNM_FNode_List * PNM_SubList_CreateAndInitalizeNewNodeList (PNM_SubList * list) {
    if (list->nodeList == NULL) {
        list->nodeList = (PNM_FNode_List *) malloc(sizeof(PNM_FNode_List));
    }
    list->nodeList->size = 0;
    list->nodeList->begin = NULL;
    list->nodeList->end = list->nodeList->begin;
    return list->nodeList;
}

/*
 * Inserts/Appends a new sub-list (PNM_SubList) at the end of list (PNM_List). 
 * Returns the current number of elements in the list.
 * @params:
 *      [list] : Global list (PNM_List) of all sub-lists containing the possible next moves for knight figure.
 *      [new_sublist] : New element (PNM_SubList) to be inserted at list end.
 */
PNM_API int PNM_List_AddSubList (PNM_List * list, PNM_SubList * new_sublist) {
    if (list == NULL) {
        return 0;
    }
    if (list->size == 0) {
        list->begin = new_sublist;
        list->end = new_sublist;
    }
    else {
        list->end->next = new_sublist;
        list->end = new_sublist;
    }
    list->end->next = NULL;
    ++(list->size);
    return list->size;
}

/*
 * Removes the last element (PNM_SubList) from the global list (PNM_List).
 * Returns a pointer to that removed element.
 */
PNM_API PNM_SubList * PNM_List_Popback (PNM_List * list) {
    if (list == NULL || list->size == 0) {
        return NULL;
    }
    PNM_SubList * subList = list->begin;
    if (list->size == 1) {
        list->begin = NULL;
        list->end = list->begin;
    }
    else { // list->size >= 2
        int k = 0;
        while (k != (list->size - 2)) { // Attempt to get the SECOND LAST element in the global list
            subList = subList->next;
            ++k;
        }
        PNM_SubList * secondLast = subList; // SECOND LAST element
        // subList = list->end;
        subList = subList->next; // Last element
        list->end = secondLast;
        list->end->next = NULL; // secondLast->next = NULL;
    }
    subList->next = NULL;
    --(list->size);
    return subList;
}

/*
 * Inserts/Appends a new element (PNM_SubList) at the end of list (PNM_List). 
 * Returns the current number of elements in the list.
 * @params:
 *      [list] : Global list (PNM_List) of all sub-lists containing the possible next moves for knight figure.
 *      [new_sublist] : New element (PNM_SubList) to be inserted at list end.
 */
PNM_API int PNM_List_Pushback (PNM_List * list, PNM_SubList * new_sublist) {
    return PNM_List_AddSubList(list, new_sublist);
}

#ifdef __cplusplus
}
#endif

#endif /* PNM_H */