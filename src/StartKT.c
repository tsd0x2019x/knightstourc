/**
 * StartKT.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "KT.h"

/*
 * If a knight's tour path is found, display it with its moves.
 */
void Print_Result (KT_Solver * solver, KT_Boolean_t result) {
    if (result == KT_TRUE) {
        printf("\n==> Yes. There exists a path.\n");
    }
    else {
        printf("==> No. There's no path.\n");
    }
    printf("\nFinish!\n\n");
}

/*
 * Display program intro
 */
void Print_Intro () {
    printf("\n***************************");
    printf("\n*                         *");
    printf("\n*      KNIGHT'S TOUR      *");
    printf("\n*                         *");
    printf("\n***************************\n\n");
}

/*
 * Display interactive input-dialogs
 */
int Display_Input_Dialog (int * dim, int * row, int * col) {
    Print_Intro();
    // Read dimension
    printf("Size of square board: ");
    if (scanf("%d", dim) <= 0 || *dim <= 0) {
        printf("Error: Invalid input for dimension. Exit!\n\n");
        return EXIT_FAILURE;
    }
    printf("Square board of dimension %dx%d is initialized.\n\n", *dim, *dim);
    // Read start row
    printf("Start row (0..%d): ", *dim-1);
    if (scanf("%d", row) < 0 || *dim <= 0) {
        printf("Error: Invalid row value. Exit!\n\n");
        return EXIT_FAILURE;
    }
    // Read start column
    printf("Start column (0..%d): ", *dim-1);
    if (scanf("%d", col) <= 0 || *dim < 0) {
        printf("Error: Invalid column value. Exit!\n\n");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;   
}

/*
 * Show help menue and usage with options.
 */
void Display_Help (const char * program_name) {
    const char * pName = program_name;
    if (*pName == '.' && pName[1] == '/') { // In Linux program is started by "./program_name". We only want the plain "program_name" without the leading dot (.) and slash (/).
        pName += 2; // Therefore, jump two characters to right.
    }
    printf("USAGE:   %s (parameters{3} | option)?\n", program_name);
    printf("DESCRIPTION: This program solves to well-known \"Knight's tour\" problem and provides a path with all moves as result.\n");
    printf("             It can be called with or without parameters. If called with parameters, it mus get exactly 3 parameters (dim, row, column) as function arguments.\n");
    printf("OPTIONS:\n");
    printf("    -d, --debug    : Debug mode. Every change of board state will be displayed.\n");
    printf("                     Can be used together with the parameters (dimension, row, colum).\n");
    printf("    -h, --help     : Show help.\n");
    printf("    -?             : Show help.\n");
    printf("EXAMPLE:\n");
    printf("  Assume that the calling program is \"%s\". Some use cases are described in the following:\n", pName);
    printf("    (1) Run program with parameters \"dim = 8, row = 0, col = 4\":\n");
    printf("            %s %d %d %d\n", program_name, 8, 0, 4);
    printf("        It will initialize a square (chess) board with %dx%d=%d squares and set the knight at square (%d,%d) before processing.\n\n", 8, 8, 64, 0, 4);
    printf("        Since no option --debug is given, the program will not display the state changes of (chess) board.\n");
    printf("    (2) Run program without parameters:\n");
    printf("            %s\n", program_name);
    printf("        It will start and display interactive dialogs for inputs.\n\n");
    printf("        Since no option --debug is given, the program will not display the state changes of (chess) board.\n");
    printf("    (3) Run program in debug mode (option --debug) and parameters \"dim = 8, row = 0, col = 4\":\n");
    printf("            %s -d %d %d %d\n", program_name, 8, 0, 4);
    printf("            %s --debug %d %d %d\n", program_name, 8, 0, 4);
    printf("            %s %d %d %d -d\n", program_name, 8, 0, 4);
    printf("            %s %d %d %d --debug\n", program_name, 8, 0, 4);
    printf("        It will initialize a square (chess) board with %dx%d=%d squares and set the knight at square (%d,%d) before processing.\n\n", 8, 8, 64, 0, 4);
    printf("        Since the option --debug (-d) is given, the program will monitor every the state change of (chess) board.\n");
    printf("    (4) Show help and usage menue:\n");
    printf("            %s --help\n", program_name);
    printf("            %s -h\n", program_name);
    printf("            %s -?\n\n", program_name);
}

/*
 * Main program. Start from here.
 * USAGE:
 *      ./StartKT         <dim> <row> <col>
 *      ./StartKT --debug <dim> <row> <col>
 *      ./StartKT         <dim> <row> <col> --debug
 *      ./StartKT -d      <dim> <row> <col>
 *      ./StartKT         <dim> <row> <col> -d
 *      ./StartKT
 * 
 * Maximum number of arguments [argc] for this program is 5, because:
 *      argv[0] is the program name, that means "./StartKT" or "StartKT.exe".
 *      argv[1] until argv[4] may be "--debug" <dim> <row> <col>.
 */
int main (int argc, char * argv[]) {

    short debug_mode = 0; // No debug
    int dim = 0; // Size of one side of the square (chess) board.
    int row = 0; // Row index where the knight will start from.
    int col = 0; // Column index where the knight will start from.

    /* Process given arguments */
    if (argc < 5) {
        if (argc == 2 && (strncmp(argv[1], "-h", 2) == 0 || strncmp(argv[1], "-?", 2) == 0 || strncmp(argv[1], "--help", 6) == 0)) {
            Display_Help(argv[0]);
            return EXIT_SUCCESS;
        }
        else if (argc == 1) {
            Display_Input_Dialog(&dim, &row, &col);
        }
        else {
            if (argc >= 2) {
                for (int i = 1; i < argc; i++) {
                    if (strncmp(argv[i], "-d", 2) == 0 || strncmp(argv[i], "--debug", 7) == 0) {
                        debug_mode = 1;
                        break;
                    }
                }
            }
            if (argc == 4 && !debug_mode) {
                dim = atoi(argv[1]);
                row = atoi(argv[2]);
                col = atoi(argv[3]);
            }
            else {
                Display_Input_Dialog(&dim, &row, &col);
            }
        }
    }
    else { // argc >= 5. We only need 5, that means 1 "program_name" plus 3 parameters for [dim], [row], [col] and 1 option "--debug".
        if (strncmp(argv[1], "-d", 2) == 0 || strncmp(argv[1], "--debug", 7) == 0) {
            debug_mode = 1;
            dim = atoi(argv[2]);
            row = atoi(argv[3]);
            col = atoi(argv[4]); 
        }
        else if (strncmp(argv[4], "-d", 2) == 0 || strncmp(argv[4], "--debug", 7) == 0) {
            debug_mode = 1;
            dim = atoi(argv[1]);
            row = atoi(argv[2]);
            col = atoi(argv[3]);
        }                
    }     
    printf("Knight starts at (%d, %d).\nProgram runs...\n\n", row, col);

    /* Create and initialize problem solver */
    KT_Solver solver;
    KT_Init(&solver, dim, debug_mode);
    /* Run solver */
    KT_Boolean_t result = KT_Solve(&solver, row, col);

    // Display result
    Print_Result(&solver, result);

    return EXIT_SUCCESS;
}