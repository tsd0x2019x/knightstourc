/**
 * KT.h (Knight's Tour)
 */
#ifndef KT_H
#define KT_H

#include "PNM.h"

#ifdef __cplusplus
extern "C" {
#endif

#define KT_API extern
//#define KT_DEBUG KT_DEBUG
#define KT_SHOW_PATH KT_SHOW_PATH

//==============================================================================================================================
typedef enum { KT_FALSE = 0, KT_TRUE = 1 } KT_Boolean_t; // Boolean type
typedef enum { KT_FREE = 0,  KT_VISITED = 1 } KT_State_t; // State of a field

//==============================================================================================================================
typedef struct {
    unsigned int dim; // Size (dimension) of the square board's side
    unsigned int squares_visited; // Number of squares/field that are busy (not free)
    KT_State_t * board; // One-dimensional array representing an two-dimensional (chess) board
    PNM_List * list; // Unique (global) list for all possible next moves
    short debug; // Debug mode (0: NO DEBUG, 1: DEBUG)
} KT_Solver;

//==============================================================================================================================

KT_API void KT_DisplayBoard (KT_Solver * solver);
KT_API void KT_PrintPath (KT_Solver * solver);
KT_API KT_State_t KT_GetStateOfField (KT_Solver * solver, int row, int col);
KT_API void KT_SetField (KT_Solver * solver, int row, int col);
KT_API void KT_UnsetField (KT_Solver * solver, int row, int col);
KT_API KT_Boolean_t KT_IsFreeField (KT_Solver * solver, int row, int col);
KT_API int KT_GetNumberOfPossibleNextMovesFromField (KT_Solver * solver, int row, int col);
KT_API void KT_SearchPossibleNextMovesFromField(KT_Solver * solver, int row, int col, PNM_SubList * subList);
KT_API KT_Boolean_t KT_Backtrack_Warnsdorf (KT_Solver * solver);
KT_API KT_Boolean_t KT_Findpath_Warnsdorf (KT_Solver * solver, int row, int col);
KT_API void KT_Release_Memory (KT_Solver * solver);
KT_API KT_Boolean_t KT_Solve (KT_Solver * solver, int start_row, int start_column);
KT_API void KT_Init (KT_Solver * solver, unsigned int dimension, short debug_mode);

//==============================================================================================================================
/*
 * Display the current state of board.
 */
KT_API void KT_DisplayBoard (KT_Solver * solver) {
    printf("\n");
    int index = 0;
    for (int row = 0; row < solver->dim; row++) {
        for (int col = 0; col < solver->dim; col++) {
            index = row * solver->dim + col;
            if (solver->board[index] == KT_VISITED) {
                printf("X ");
            }
            else {
                printf("O ");
            }
        }
        printf("\n");
    }
    printf("\n");
}

/*
 * Print the (row,col) coordinates of the global list.
 */
KT_API void KT_PrintPath (KT_Solver * solver) {
    if (solver->list != NULL && solver->list->size > 0) {
        PNM_SubList * subList = solver->list->begin;
        printf(" $");
        do {
            printf("->(%d,%d)", subList->current.row, subList->current.col);
            subList = subList->next;
        } while (subList != NULL);
        printf("\n");
    }
}

/*
 * Provide the current state of the field (row,col). Return KT_BUSY (1) if this field is not free. Otherwise, return KT_FREE (0).
 * Mapping two-dimensional (row,col) to one-dimensional [index]:
 *      (row,col) |--> [index] := row * dim + col
 * where dim is the size (dimension) of the square board's side.
 * Reverse mapping one-dimensional [index] to two-dimensional (row,col):
 *      [index] |--> (row := index / dim, col := index % dim)
 * 
 * @params:
 *      [solver] : 
 *      [row] : 
 *      [col] :
 */
KT_API KT_State_t KT_GetStateOfField (KT_Solver * solver, int row, int col) {
    int index = row * solver->dim + col;
    return solver->board[index];
}

/*
 * Set a field (row,col) to KT_BUSY (1). If this field is KT_FREE (0), set it to KT_BUSY. Otherwise, nothing to do.
 * Mapping two-dimensional (row,col) to one-dimensional [index]:
 *      (row,col) |--> [index] := row * dim + col
 * where dim is the size (dimension) of the square board's side.
 * Reverse mapping one-dimensional [index] to two-dimensional (row,col):
 *      [index] |--> (row := index / dim, col := index % dim)
 * 
 * @params:
 *      [solver] : 
 *      [row] : 
 *      [col] :
 */
KT_API void KT_SetField (KT_Solver * solver, int row, int col) {
    int index = row * solver->dim + col;
    solver->board[index] = KT_VISITED;
    if (solver->squares_visited < (solver->dim * solver->dim)) solver->squares_visited++;
}

/*
 * Set a field (row,col) to KT_FREE (0). If this field is KT_BUSY (1), set it to KT_FREE. Otherwise, nothing to do.
 * Mapping two-dimensional (row,col) to one-dimensional [index]:
 *      (row,col) |--> [index] := row * dim + col
 * where dim is the size (dimension) of the square board's side.
 * Reverse mapping one-dimensional [index] to two-dimensional (row,col):
 *      [index] |--> (row := index / dim, col := index % dim)
 * 
 * @params:
 *      [solver] : 
 *      [row] : 
 *      [col] :
 */
KT_API void KT_UnsetField (KT_Solver * solver, int row, int col) {
    int index = row * solver->dim + col;
    solver->board[index] = KT_FREE;
    if (solver->squares_visited > 0) solver->squares_visited--;
}

/*
 * Check if the given field (row,col) is a FREE and valid field so that the knight can move to.
 * Return TRUE (1) if field is free (valid), and FALSE (0) otherwise.
 * @params:
 *      [solver] : 
 *      [row] : The row index of the field (row,col) to be checked if it's free
 *      [col] : The column index of the field (row,col) to be checked if it's free
 * @return:
 *      KT_TRUE (1) if the given field (row,col) is free. Otherwise, KT_FALSE (0).
 */
KT_API KT_Boolean_t KT_IsFreeField (KT_Solver * solver, int row, int col) {
    if (row < 0 || row >= solver->dim || col < 0 || col >= solver->dim) return KT_FALSE; // ArrayOutOfRange exception
    if (KT_GetStateOfField(solver, row, col) != KT_FREE) return KT_FALSE;
    return KT_TRUE;
}

/*
 * Provide the number of all possible next moves from field (row,col).
 */
KT_API int KT_GetNumberOfPossibleNextMovesFromField (KT_Solver * solver, int row, int col) {
    int num = 0;    
    // 1st possible next move
	if (KT_IsFreeField(solver, row-2, col-1))  ++num;
	// 2nd possible next move
	if (KT_IsFreeField(solver, row-1, col-2))  ++num;
	// 3rd possible next move
	if (KT_IsFreeField(solver, row+1, col-2))  ++num;
	// 4th possible next move
	if (KT_IsFreeField(solver, row+2, col-1))  ++num;
	// 5th possible next move
	if (KT_IsFreeField(solver, row+2, col+1))  ++num;
	// 6th possible next move
	if (KT_IsFreeField(solver, row+1, col+2))  ++num;
	// 7th possible next move
	if (KT_IsFreeField(solver, row-1, col+2))  ++num;
	// 8th possible next move
	if (KT_IsFreeField(solver, row-2, col+1))  ++num;
    return num;
}

/*
 * Looks for the possible next moves which the knight figure can jump to from it's current position/field (row,col).
 */
KT_API void KT_SearchPossibleNextMovesFromField (KT_Solver * solver, int row, int col, PNM_SubList * subList) {
    // 1st possible next move
	if (KT_IsFreeField(solver, row-2, col-1)) PNM_FNode_List_AddField(subList->nodeList, row-2, col-1);
	// 2nd possible next move
	if (KT_IsFreeField(solver, row-1, col-2)) PNM_FNode_List_AddField(subList->nodeList, row-1, col-2);
	// 3rd possible next move
	if (KT_IsFreeField(solver, row+1, col-2)) PNM_FNode_List_AddField(subList->nodeList, row+1, col-2);
	// 4th possible next move
	if (KT_IsFreeField(solver, row+2, col-1)) PNM_FNode_List_AddField(subList->nodeList, row+2, col-1);
	// 5th possible next move
	if (KT_IsFreeField(solver, row+2, col+1)) PNM_FNode_List_AddField(subList->nodeList, row+2, col+1);
	// 6th possible next move
	if (KT_IsFreeField(solver, row+1, col+2)) PNM_FNode_List_AddField(subList->nodeList, row+1, col+2);
	// 7th possible next move
	if (KT_IsFreeField(solver, row-1, col+2)) PNM_FNode_List_AddField(subList->nodeList, row-1, col+2);
	// 8th possible next move
	if (KT_IsFreeField(solver, row-2, col+1)) PNM_FNode_List_AddField(subList->nodeList, row-2, col+1);
}

/*
 * Backtracking with Warnsdorf's rule.
 * Warnsdorfs Rule: Der Springer zieht immer auf das Feld, von dem aus er für seinen nächsten Zug 
 *                  am wenigsten freie (d.h. noch nicht besuchte) Felder zur Verfügung hat.
 *                  The knight is moved so that it always proceeds to the square from which the 
 *                  knight will have the fewest onward moves.
 */
KT_API KT_Boolean_t KT_Backtrack_Warnsdorf (KT_Solver * solver) {
    // Variable to store result
    KT_Boolean_t Result = KT_FALSE;
    // Get reference/pointer to the last sub-list.
    PNM_SubList * last_subList = solver->list->end;
    // Remove the last element (PNM_SubList) from the global list (PNM_List). Get reference to this element.
    //PNM_SubList * last_subList = PNM_List_Popback(solver->list);
    // Get row and column index of current field/square.
    int row = last_subList->current.row;
    int col = last_subList->current.col;
    // Distinction of cases
    if (last_subList->nodeList == NULL || last_subList->nodeList->size == 0) {
        // Unset this field/square, i.e. set it to KT_FREE.
        KT_UnsetField(solver, row, col);
        // Remove the last element (PNM_SubList) from the global list (PNM_List). Get reference to this element.
        last_subList = PNM_List_Popback(solver->list);
        if (last_subList->nodeList != NULL) {
            free(last_subList->nodeList); // Destroy nodeList
            last_subList->nodeList = NULL;
        }
        // Debug mode
        if (solver->debug) {
            KT_DisplayBoard(solver);
        }
        free(last_subList); // Destroy subList
        last_subList = NULL;
        if (solver->list->size == 0) { // FAILURE. All possible moves have been checked.
            //Result = KT_FALSE; // There is NO path!
            return KT_FALSE;
        }
        else { // (solver->list->size) > 0
            Result = KT_Backtrack_Warnsdorf (solver); // Recursive call
        }
    }
    else { // (last_subList->nodeList->size > 0) && (last_subList->nodeList != NULL)
        // Apply Warnsdorf's rule to determine the fnode
        PNM_FNode * fnode = last_subList->nodeList->begin;
        int m = KT_GetNumberOfPossibleNextMovesFromField(solver, fnode->field.row, fnode->field.col);
        int c = 0; // Counter
        int index = 0; // Node Index
        while (fnode->next != NULL) { // Warnsdorf's rule
            fnode = fnode->next;
            c += 1;
            int mm = KT_GetNumberOfPossibleNextMovesFromField(solver, fnode->field.row, fnode->field.col);
            if (mm < m) {
                m = mm; // update m
                index = c; // update index             
            }
        }
        // Once we've found the index of the field/square from which the knight will have the FEWEST onward moves,
        // we remove this field from the list (nodeList) and obtain a pointer to it. As explained above, since N = num > 1, 
        // the nodeList will not become empty immediately after our remove action. So we don't to care about freeing the nodeList.
        fnode = PNM_FNode_List_RemoveNodeAtIndex(last_subList->nodeList, index);
        if (fnode == NULL) {
            printf("Error (KT_Backtrack_Warnsdorf): Either the index is out of range. Or the NodeList is NULL or empty.\n");
            exit(EXIT_FAILURE);
        }
        // Recursive call of KT_Findpath_Warnsdorf
        Result = KT_Findpath_Warnsdorf(solver, fnode->field.row, fnode->field.col);
        // Release allocated memory
        free(fnode); // Destroy fnode
        fnode = NULL;
    }
    return Result;
}

/*
 * (Wikipedia)
 * Warnsdorfs Rule: Der Springer zieht immer auf das Feld, von dem aus er für seinen nächsten Zug 
 *                  am wenigsten freie (d.h. noch nicht besuchte) Felder zur Verfügung hat.
 *                  The knight is moved so that it always proceeds to the square from which the 
 *                  knight will have the fewest onward moves.
 */
KT_API KT_Boolean_t KT_Findpath_Warnsdorf (KT_Solver * solver, int row, int col) {
    // Variable to store result
    KT_Boolean_t Result = KT_FALSE;
    // Mark this current field (row,col) as busy (not moveable)
    KT_SetField(solver, row, col);
    // Debug mode
    if (solver->debug) {
        KT_DisplayBoard(solver);
    }
    // Get the number of possible next moves from the current field (row,col)
    int N = KT_GetNumberOfPossibleNextMovesFromField(solver, row, col);
    // Create a sub-list (PNM_SubList) instance that contains the current field (row,col) and the sub-sub-list (PNM_FNode_List)
    PNM_SubList * newSubList = PNM_SubList_CreateNewInstance(row, col, 0);
    // Add this sub-list at the end of global list (of solver)
    PNM_List_AddSubList(solver->list, newSubList);
    if (N > 0) {
        if (N > 1) { // There are more than one possible next moves from current field/square (row,col).
            // Allocate memory for nodeList to store the found possible next moves.
            //newSubList->nodeList = (PNM_FNode_List *) malloc(sizeof(PNM_FNode_List));
            PNM_SubList_CreateAndInitalizeNewNodeList(newSubList);
            // Search for the possible next moves from the current position/field (row,col) and stores them to the sub-list.
            KT_SearchPossibleNextMovesFromField(solver, row, col, newSubList);
            // Get number of possible next moves that are found.
            //int num = newSubList->nodeList->size; // This number (num) should be equal N.
            // Since num = N and N > 1, it must hold: num > 1. So the nodeList won't be empty after we remove only one element 
            // from it. Hence, we don't need to care about freeing/releasing empty nodeList now.
            // In the following, we apply the Warnsdorf's rule.
            PNM_FNode * fnode = newSubList->nodeList->begin;
            int m = KT_GetNumberOfPossibleNextMovesFromField(solver, fnode->field.row, fnode->field.col);
            int c = 0; // Counter
            int index = 0; // Node Index
            while (fnode->next != NULL) { // Warnsdorf's rule
                fnode = fnode->next;
                c += 1;
                int mm = KT_GetNumberOfPossibleNextMovesFromField(solver, fnode->field.row, fnode->field.col);
                if (mm < m) {
                    m = mm;
                    index = c;                
                }
            }
            // Once we've found the index of the field/square from which the knight will have the FEWEST onward moves,
            // we remove this field from the list (nodeList) and obtain a pointer to it. As explained above, since N = num > 1, 
            // the nodeList will not become empty immediately after our remove action. So we don't to care about freeing the nodeList.
            fnode = PNM_FNode_List_RemoveNodeAtIndex(newSubList->nodeList, index);
            if (fnode == NULL) {
                printf("Error (KT_Findpath_Warnsdorf): Either the index is out of range. Or the NodeList is NULL or empty.\n");
                exit(EXIT_FAILURE);
            }
            // Copy relevant data
            int tmp_row = fnode->field.row;
            int tmp_col = fnode->field.col;
            // Release allocated memory
            free(fnode); // Destroy fnode
            fnode = NULL;
            // Recursive call of KT_Findpath_Warnsdorf
            Result = KT_Findpath_Warnsdorf(solver, tmp_row, tmp_col);
        }
        else { // N == 1
            // 1st possible next move
	        if (KT_IsFreeField(solver, row-2, col-1))       Result = KT_Findpath_Warnsdorf(solver, row-2, col-1);
	        // 2nd possible next move
	        else if (KT_IsFreeField(solver, row-1, col-2))  Result = KT_Findpath_Warnsdorf(solver, row-1, col-2);
	        // 3rd possible next move
	        else if (KT_IsFreeField(solver, row+1, col-2))  Result = KT_Findpath_Warnsdorf(solver, row+1, col-2);
	        // 4th possible next move
	        else if (KT_IsFreeField(solver, row+2, col-1))  Result = KT_Findpath_Warnsdorf(solver, row+2, col-1);
	        // 5th possible next move
	        else if (KT_IsFreeField(solver, row+2, col+1))  Result = KT_Findpath_Warnsdorf(solver, row+2, col+1);
	        // 6th possible next move
	        else if (KT_IsFreeField(solver, row+1, col+2))  Result = KT_Findpath_Warnsdorf(solver, row+1, col+2);
	        // 7th possible next move
	        else if (KT_IsFreeField(solver, row-1, col+2))  Result = KT_Findpath_Warnsdorf(solver, row-1, col+2);
	        // 8th possible next move
	        else if (KT_IsFreeField(solver, row-2, col+1))  Result = KT_Findpath_Warnsdorf(solver, row-2, col+1);
        }
    }
    else { // N == 0. There are no next moves possible from current square/position (row,col).
        if (solver->squares_visited == (solver->dim * solver->dim) && solver->squares_visited == solver->list->size) { // SUCCESS. All squares/fields are visited.
            Result = KT_TRUE;  // PATH is found!
#ifdef KT_SHOW_PATH
    KT_PrintPath(solver);
#endif
        }
        else { // Backtracking
            KT_Backtrack_Warnsdorf(solver);
        }
    }
    return Result;
}

/*
 * Release/Free all allocated memory.
 */
KT_API void KT_Release_Memory (KT_Solver * solver) {
    if (solver->list->size > 0 && solver->list->begin != NULL) {
        PNM_SubList * subList = solver->list->begin;
        do {
            solver->list->begin = subList->next;
            // Erase nodeList and its fields/elements
            PNM_FNode_List * nodeList = subList->nodeList;
            if (nodeList != NULL) {
                if (nodeList->begin != NULL) {
                    PNM_FNode * fnode = nodeList->begin;
                    do {
                        nodeList->begin = fnode->next;
                        fnode->next = NULL;
                        free(fnode); // Release memory
                        fnode = nodeList->begin;
                        --(nodeList->size);
                    } while (fnode != NULL);
                    nodeList->begin = nodeList->end = fnode = NULL;
                }
            }
            //nodeList->size = 0;
            free(nodeList); // Release memory
            nodeList = NULL;
            // Erase subList
            free(subList); // Release memory
            subList = solver->list->begin;
            --(solver->list->size);
        } while (subList != NULL);
        subList = NULL;
    }
    //solver->list->size = 0;
    free(solver->list); solver->list = NULL;
    free(solver->board); solver->board = NULL;    
}

/*
 *
 */
KT_API KT_Boolean_t KT_Solve (KT_Solver * solver, int start_row, int start_column) {
    if (start_row < 0 || start_row >= solver->dim || start_column < 0 || start_column >= solver->dim) {
        return KT_FALSE;
    }
    /* Create the (chess) board */
    int total = solver->dim * solver->dim;
    solver->board = (KT_State_t *) malloc(sizeof(KT_State_t) * total);
    for (int i = 0; i < total; i++) {
        solver->board[i] = KT_FREE;
    }
#ifdef KT_DEBUG
    KT_DisplayBoard(solver);
#endif
    /* Create the unique (global) list (PNM_List) object for storing all possible next moves */
    solver->list = (PNM_List *) malloc(sizeof(PNM_List));
    solver->list->size = 0;
    solver->list->begin = solver->list->end = NULL;
    /* Solve the Knight's Tour problem */
    KT_Boolean_t result = KT_Findpath_Warnsdorf(solver, start_row, start_column);
    /* Release all allocated memory */
    KT_Release_Memory(solver);
    return result;
}

/*
 *
 */
KT_API void KT_Init (KT_Solver * solver, unsigned int dimension, short debug_mode) {
    solver->dim = dimension;
    solver->squares_visited = 0;
    solver->board = NULL;
    solver->list = NULL;
    solver->debug = debug_mode;
}

#ifdef __cplusplus
}
#endif

#endif /* KT_H */
